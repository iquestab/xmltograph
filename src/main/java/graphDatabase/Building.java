package graphDatabase;

import java.util.Arrays;

import org.neo4j.fabric.stream.SourceTagging;

public class Building {

	private static final String[] HEATING = new String[]{"VS", "VP"};
    private static final String VENTILATION = "LB";
    private static final String HOT_WATER = "VV";
    private static final String PRESSURE = "GP";
    private static final String TEMPERATURE = "GT";

	private final String uuid;
	private final String as;
	private final String building;	
	private final String node;
	private final String nodeType;
	private final String system;
	private final String systemType;
	private final String qualifier;
	
	public Building(final String uuid, final String as, final String building,  final String node, final String system, final String qualifier) {
		
		this.uuid = uuid;
		this.as = as;
		this.building = building;
		this.node = node;
		this.system = system;
		this.qualifier = qualifier;

		//Specifying the systemType of the system
		if (this.system.substring(0,2).equals(VENTILATION)) {
			this.systemType = "ventilation";
		}
		else if(this.system.substring(0,2).equals(HOT_WATER)){
			this.systemType = "hot water";
		}
		else if(Arrays.asList(HEATING).contains(this.system.substring(0,2))){
			this.systemType = "heating";
		}
		else{
			this.systemType="as";
		}


		//Specifying the nodeType of the system
		if (this.node.substring(0,2).equals(TEMPERATURE)) {
			this.nodeType = "temperature";
		}
		else if(this.node.substring(0,2).equals(PRESSURE)){
			this.nodeType = "pressure";
		}
		else{
			this.nodeType="Unknown";
		}
	}
	
	public String getUuid() {
		return this.uuid;
	}
	
	public String getAs() {
		return this.as;
	}

	public String getBuilding() {
		return this.building;
	}

	public String getNode() {
		return this.node;
	}

	public String getNodeType() {
		return this.nodeType;
	}

	public String getSystem() {
		return this.system;
	}

	public String getSystemType() {
		return this.systemType;
	}

	public String getQualifier() {
		return this.qualifier;
	}
	
	
	public String toString() { 
	    return "uuid: " + this.uuid + "\nDevice: " + this.as + "\nBuilding: " + 
				this.building + "\nSystem: " + this.system + "\nSystemType: " + this.systemType + 
				"\nNode: " + this.node + "\nNodeType: " + this.nodeType +"\nQualifier: " + this.qualifier;
	} 
}
