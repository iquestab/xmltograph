package graphDatabase;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StAXParser {
	
    int id;
	
	private static final String ELEMENT_TAGS = "tags";
	private static final String ELEMENT_KEY = "key";
	private static final String ELEMENT_VALUE = "value";
	
	private final String fileName;
    private final XMLInputFactory factory = XMLInputFactory.newInstance();
    private final List<Building> buildingList;
	
	public StAXParser(final String fileName, final List<Building> buildingList) {
		this.fileName = fileName;
		this.buildingList = buildingList;
	}
	
	public List<Building> parse() throws IOException, XMLStreamException {
	    
	    String uuid = null;
		
        try(final InputStream stream = this.getClass().getResourceAsStream(fileName)) {
            
            
            
                 final XMLEventReader reader = factory.createXMLEventReader(stream);
                
                while (reader.hasNext()) {
                    
                    final XMLEvent event = reader.nextEvent();
                    
                    if (event.isStartElement() && ((event.asStartElement().getName()
                          .getLocalPart().equals("modbus-poller")) || (event.asStartElement().getName()
                          .getLocalPart().equals("mssql-miner")))) {
                        
                        Iterator<Attribute> attributes = event.asStartElement().getAttributes();
                        while(attributes.hasNext()){
                            Attribute attr = attributes.next();
                            if(attr.getName().toString().equals("uuid")){
                                uuid = attr.getValue();
                            }
                        }
                    }

                    if (event.isStartElement() && event.asStartElement().getName()
                            .getLocalPart().equals(ELEMENT_TAGS)) {
                        parseTags(reader, uuid);
                    }
            }
            return buildingList;
        }
    }
	
	private void parseTags(final XMLEventReader reader, String uuid) throws XMLStreamException {
        
        final ArrayList<String> keys = new ArrayList<String>();
        final ArrayList<String> values = new ArrayList<String>();
        
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(ELEMENT_TAGS)) {
                final Building building;

            	final Map<String, String> key_values = new HashMap<String, String>();
            	
            	for (int i = 0; i < keys.size(); i++) {
                    key_values.put(keys.get(i), values.get(i));
                }
            	

                if (keys.contains("as")) {
                    building = new Building(uuid, key_values.get("as"), key_values.get("building"), 
            			key_values.get("node"), key_values.get("system"), key_values.get("qualifier"));
                }
                else{
                    building = new Building(uuid, key_values.get("device"), key_values.get("building"), 
            			key_values.get("node"), key_values.get("system"), key_values.get("qualifier"));
                }
            	
            	buildingList.add(building);
            	System.out.println(building);
            	System.out.println();
            }
            
            if (event.isStartElement()) {
                final StartElement element = event.asStartElement();
                final String elementName = element.getName().getLocalPart();

                switch (elementName) {

                case ELEMENT_KEY:
                	keys.add(reader.getElementText().replaceAll("\\W", ""));
                	break;
                case ELEMENT_VALUE:
                	values.add(reader.getElementText());
                	break;
                }	
			}
    	}     
     }

}
