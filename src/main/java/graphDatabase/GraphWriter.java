package graphDatabase;

import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;

public class GraphWriter implements AutoCloseable {
	
	private final Driver driver;
	
	public GraphWriter(String uri, String username, String password) {
		
		this.driver = GraphDatabase.driver( uri, AuthTokens.basic( username, password ) );
	}

	@Override
	public void close() throws Exception {
		driver.close();
	}
	

	
	public void addGraphNodes(Building building) {

		try (Session session = driver.session())
        {   

			String query = String.format("MERGE (a:building {name:'%s', uuid:'%s'})\nMERGE (b:device {name:'%s', uuid:'%s'})\n"
					+ "MERGE (c:system {name:'%s', type:'%s', uuid:'%s'})\nMERGE (d:node {name:'%s', type:'%s', uuid:'%s'})\n"
					+ "MERGE (e:qualifier {name:'%s', uuid:'%s'})", 
					building.getBuilding(),building.getUuid(), building.getAs(), building.getUuid(), building.getSystem(), building.getSystemType(),
					building.getUuid(), building.getNode(), building.getNodeType(), building.getUuid(), building.getQualifier(), building.getUuid());
			session.run(query);
            
        }
	}
	
	public void addRelationship(Building building) {
		
		try (Session session = driver.session())
        {   
			String query = String.format("MATCH (a:building {name:'%s'}),(b:device {name:'%s'}) ,"
					+ "(c:system {name:'%s'}), (d:node {name:'%s'}), (e:qualifier {name:'%s'})\n"
					+ "MERGE (a)-[r1:HAS]->(b)\nMERGE (b)-[r2:HAS]->(c)\n"
					+ "MERGE (c)-[r3:HAS]->(d)\nMERGE (d)-[r4:HAS]->(e)", 
					building.getBuilding(), building.getAs(), building.getSystem(), 
						building.getNode(), building.getQualifier());
			session.run(query);
            
        }				
	}
	
	public void search() {
		try (Session session = driver.session())
       {   
			Result result = session.run("MATCH (a:node)-[]->(b:qualifier {name:'MV'})\nRETURN a.name");

			while (result.hasNext()) {
				System.out.println(result.next());

			}
			
       }		
	}
}
