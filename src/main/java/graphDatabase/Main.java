package graphDatabase;

import java.util.ArrayList;
import java.util.List;


public class Main {

	public static void main(String[] args) throws Exception {
		
		GraphWriter gw = new GraphWriter("bolt://localhost:7687", "neo4j" , "amir");
		String file = "/poller_Sandbacken_AS01.xml";
		// file=file.replace(" ", "_");
		// file=file.replace(",", "_");

		
	// 	if (args.length > 0) 
    //    {  
    //        for (String val:args) {
	// 			file = val.replace(" ", "_");
	// 			file = file.replace(",", "_");
    //        }                
    //    } 
    //    else {
    //        System.out.println("Error: please add a XML file");
    //        System.exit(0);
    //    }

		
		List<Building> buildingList = new ArrayList<Building>();
		
		
		StAXParser parser = new StAXParser(file,  buildingList);
		buildingList = parser.parse();
		
		for (int i = 0; i < buildingList.size(); i++) {
			gw.addGraphNodes(buildingList.get(i));
			gw.addRelationship(buildingList.get(i));
		}
		System.out.println("Done");
		gw.close();
	}
}
